"use strict";

function refreshUI() {
    payValueSpan.innerText = String(this._pay);
    loanValueSpan.innerText = String(this._loan);
    this._availableMoney = this._loan + this._credit;
    balanceValueSpan.innerText = String(this._availableMoney);
    creditValueSpan.innerText = String(this._credit);
    repayLoanButton.hidden = this._loan > 0 ? false : true;
}

class Person {
    constructor() {
        this._credit = 200;
        this._loan = 0;
        this._laptops = [];
        this._pay = 0;
        this._availableMoney = 0;
    }
    get credit() {
        return this._credit;
    }
    set credit(value) {
        this._credit = value;
    }
    get loan() {
        return this._loan;
    }
    set loan(value) {
        this._loan = value;
    }
    get laptops() {
        return this._laptops;
    }
    set laptops(value) {
        this._laptops = value;
    }
    get pay() {
        return this._pay;
    }
    set pay(value) {
        this._pay = value;
    }
    get availableMoney() {
        return this._availableMoney;
    }
    set availableMoney(value) {
        this._availableMoney = value;
    }
    aquireLoan() {
        let amountString = prompt("How much?");
        let amount = +amountString;
        console.log(this._loan);
        if (amount > this._credit * 2 || this._loan > 0 || amount <= 0) {
            alert("Loan not granted ");
        }
        else {
            this._loan = amount;
            alert("Loan granted");
        }
        if (this._loan > 0) {
            loanSpan.hidden = false;
            loanValueSpan.innerText = String(this._loan);
            repayLoanButton.hidden = false;
        }
        else {
            loanSpan.hidden = true;
        }
        this._availableMoney = this._loan + this._credit;
        balanceValueSpan.innerText = String(this._availableMoney);
        creditValueSpan.innerText = String(this._credit);
    }
    payOut() {
        if (this._loan > 0) {
            let diff = this._loan - this._pay * 0.1;
            if (diff < 0) {
                this._credit += diff * (-1);
                this._loan = 0;
            }
            else {
                this._loan = diff;
            }
            this._credit += 0.9 * this._pay;
            this._pay = 0;
        }
        else {
            this._credit += this._pay;
            this._pay = 0;
        }
        refreshUI.call(this);
    }
    work() {
        let increase = 100;
        this._pay += increase;
        payValueSpan.innerText = String(this._pay);
    }
    repayLoan() {
        let diff = this._loan - this._pay;
        if (diff > 0) {
            this._loan = diff;
        }
        else {
            this._loan = 0;
            this._credit += diff * (-1);
        }
        this._pay = 0;
        payValueSpan.innerText = String(this._pay);
        loanValueSpan.innerText = String(this._loan);
        this._availableMoney = this._loan + this._credit;
        balanceValueSpan.innerText = String(this._availableMoney);
        creditValueSpan.innerText = String(this._credit);
        repayLoanButton.hidden = this._loan > 0 ? false : true;
    }
}
let person = new Person();
//Buttons
const getLoanButton = document.getElementById("getLoanButton");
const bankButton = document.getElementById("bankButton");
const workButton = document.getElementById("workButton");
const repayLoanButton = document.getElementById("repayLoanButton");
const buyNowButton = document.getElementById("buyNowButton");
//Spans
const loanValueSpan = document.getElementById("loan");
const payValueSpan = document.getElementById("pay");
const balanceValueSpan = document.getElementById("balance");
const creditValueSpan = document.getElementById("credit");
const loanSpan = document.getElementById("loanSpan");
const paySpan = document.getElementById("paySpan");
const priceSpan = document.getElementById("priceSpan");
//Selects
const laptopSelect = document.getElementById("laptopSelect");
//paragraphs
const featuresParagraph = document.getElementById("featuresParagraphSelection");
const laptopDescriptionInfoArea = document.getElementById("laptopDescriptionInfoArea");
//images
const laptopImage = document.getElementById("laptopImage");
//h1
const laptopNameH1 = document.getElementById("laptopNameH1");
getLoanButton.addEventListener("click", person.aquireLoan.bind(person));
bankButton.addEventListener("click", person.payOut.bind(person));
workButton.addEventListener("click", person.work.bind(person));
repayLoanButton.addEventListener("click", person.repayLoan.bind(person));
laptopSelect.addEventListener("change", event => showInfoOfSelctedLaptop(event));
buyNowButton.addEventListener("click", buyLaptop.bind(person));
function buyLaptop() {
    // @ts-ignore
    let price = laptops[laptopSelect.value - 1].price;
    console.log("price " + price);
    // @ts-ignore
    if (this.availableMoney >= price) {
        this.credit -= price;
        alert("Bought Laptop for " + price + " Euros");
    }
    else {
        alert("Not enough money available");
    }
    payValueSpan.innerText = String(this.pay);
    loanValueSpan.innerText = String(this.loan);
    this.availableMoney = this.loan + this.credit;
    balanceValueSpan.innerText = String(this.availableMoney);
    creditValueSpan.innerText = String(this.credit);
    repayLoanButton.hidden = this.loan > 0 ? false : true;
}
function showInfoOfSelctedLaptop(event) {
    console.log(`target value: ` + event.target.value);
    let laptopId = event.target.value;
    let laptop = laptops[laptopId - 1];
    // @ts-ignore
    laptopImage.setAttribute("src", "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image);
    // @ts-ignore
    laptopDescriptionInfoArea.innerText = laptop.description;
    // @ts-ignore
    priceSpan.innerText = laptop.price;
    // @ts-ignore
    laptopNameH1.innerText = laptop.title;
    // @ts-ignore
    featuresParagraph.innerText = laptop.specs;
}
let laptops;
function getLaptops(fetched) {
    laptops = fetched;
}
function updateUI() {
    laptopSelect.innerHTML = "";

    for (let laptop of laptops) {
        let option = document.createElement("option");
        // @ts-ignore
        option.value = laptop.id;
        // @ts-ignore
        option.text = laptop.title;
        laptopSelect.appendChild(option);
    }
}
function updateData() {
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        .then(response => response.json())
        .then(result => getLaptops(result))
        .then(() => updateUI())
        .catch(() => alert("Update UI Failed"));
}
updateData();
