# Assignment-4: computer store
This application implements a little computer store with typescript, html and css

## Usage

The application is deployed at: https://assignment-4-loibl.herokuapp.com/

If you want to run it yourself, you simpy need to clone the repo 
and open index.html in your favourite browser

## UI
you can see how the page looks in the 
<details>

![](screenshot.jpg)
</details>

## Maintainer

[Philipp Loibl]
## License
[MIT]
---

[Philipp Loibl]: https://github.com/Loibl33
[MIT]: https://choosealicense.com/licenses/mit/
